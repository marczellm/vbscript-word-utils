' V�ltoz�k deklar�l�sa
Option Explicit
Dim Word, Document, FolderPath, FilePath, FileSystem, Edit, FileList, InfoString
Const wdStyleHeading1 = -2 'WdBuiltinStyle.wdStyleHeading enum �rt�ke, csak VBScriptben nincs enum

' A & oper�tor a stringek �sszef�z�se
' Az _ underscore (al�h�z�s) karakter ahhoz kell, hogy az utas�t�s t�bbsoros lehessen
InfoString = "A k�vetkez� fog t�rt�nni: " & vbCrLf &_
	   "Az aktu�lis mapp�ban egy �j f�jlba tartalomjegyz�ket k�sz�tek a Word dokumentumokr�l, felt�ve, hogy" & vbCrLf &_
	   "1. a sz�m�t�g�pen telep�tve van a Word" & vbCrLf &_
	   "2. a f�jlok els� sora minden esetben a dokumentum c�me" & vbCrLf &_
	   "3. a f�jlok elnevez�se a k�vetkez� s�m�t k�veti:" & vbCrLf &_
	   " 4 tetsz�leges karakter + 1 sz�mjegy a fejezet sorsz�ma + ak�rmennyi sz�mjegy a kezd� oldal sorsz�ma + .doc vagy .docx"

' El�sz�r t�j�koztatjuk a felhaszn�l�t :)
If MsgBox(InfoString,1) = 2 Then
	WScript.Quit
End If

' Megnyitjuk a f�jlkezel�t �s elk�r�nk n�h�ny adatot
Set FileSystem = CreateObject("Scripting.FileSystemObject")
FolderPath     = FileSystem.GetAbsolutePathName(".")            ' aktu�lis mappa el�r�si �tja
FilePath       = FolderPath & "\Tartalomjegyz�k.doc"
Set FileList   = FileSystem.GetFolder(FolderPath).files

' Ha m�r van tartalomjegyz�k, t�r�lj�k, k�l�nben beker�lne az �j tartalomjegyz�kbe
If FileSystem.FileExists(FilePath) Then
	FileSystem.DeleteFile(FilePath)
End If

' Megnyitjuk a Word�t �s egy �j dokumentumot
Set Word       = CreateObject("Word.Application")
Set Document   = Word.Documents.Add
Set Edit       = Word.Selection
Word.Visible   = True

' A dokumentum nyelv�t magyarra �ll�tjuk
Edit.Range.WholeStory      ' mindent kijel�l
Edit.LanguageID    = 1038
Edit.NoProofing    = False ' ez nem tudom, mi, de kell
Word.CheckLanguage = True  ' szint�n

Edit.style = wdStyleHeading1
Edit.TypeText("Tartalomjegyz�k")
Edit.TypeParagraph

Dim Word2, Subfile, Subdoc, Chapter, Page, Title, LinkText
Set Word2           = CreateObject("Word.Application")
Word2.Visible       = False
Word2.DisplayAlerts = False

' �sszegy�jtj�k a f�jlok nev�t �s c�m�t
For Each Subfile in FileList
	If LCase(Right(Subfile.Name,3)) = "doc" Or LCase(Right(Subfile.Name,4)) = "docx" Then
		Set Subdoc = Word2.Documents.Open(Subfile.Path,,True)
		Chapter    = Mid(Subfile.Name,5,1)
		Page       = Mid(Subfile.Name,6,InStr(Subfile.Name,".") - 6)
		Title      = Subdoc.Paragraphs(1).Range.Text
		LinkText   = Chapter & "." & Page & " " & Title
		Document.Hyperlinks.Add Edit.Range, Subfile.Name,,, LinkText ' itt sajnos nem engedi a z�r�jeles szintaxist
		Subdoc.Close
	End If
Next
' a Next a "ciklus v�ge"

Word2.Quit
Document.SaveAs(FilePath)
MsgBox("A tartalomjegyz�k elk�sz�lt! A Ctrl nyomvatart�sa mellett kattinthatsz a hivatkoz�sokra!")

' Bocsi, hogy nem JavaScriptben �rtam; �ppen ehhez volt kedvem :)