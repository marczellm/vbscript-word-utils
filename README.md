### maketoc.vbs

This script assumes a certain file naming convention and creates a table of contents with linked entries of all files conforming to said convention.

### replaceacutes.vbs

This script replaces all occurrences of characters õ and û to ő and ű in all Word documents in the current folder.