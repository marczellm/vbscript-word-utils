' -*- coding: utf-16le-with-signature-dos -*-
Option Explicit
Dim Word, Document, FolderPath, FileSystem, FileList, File, Doc, InfoString, IE
Const Hidden = 2
Const wdFindContinue = 1
Const wdReplaceAll = 2
Const wdOriginalDocumentFormat = 1

InfoString = "Ez a szkript az aktuális mappában lévő összes Word dokumentumban kicseréli az õ és û karaktereket ő és ű-re" & vbCrLf & "Mehet?"
If MsgBox(InfoString,1) = 2 Then
	WScript.Quit
End If

Set FileSystem = CreateObject("Scripting.FileSystemObject")
Set IE         = CreateObject("InternetExplorer.Application")
FolderPath     = FileSystem.GetAbsolutePathName(".")
Set FileList   = FileSystem.GetFolder(FolderPath).files
Set Word       = CreateObject("Word.Application")
Word.Visible   = False
Word.DisplayAlerts = False

IE.Navigate("about:blank")
IE.ToolBar = False
IE.StatusBar = False
IE.Width = 400
IE.Height = 200
IE.Visible = True
IE.Document.Title = "Aktuális fájl"

For Each File in FileList
	If (LCase(Right(File.Name,3)) = "doc" Or LCase(Right(File.Name,4)) = "docx") And Not Left(File.Name,1) = "~" Then
	        IE.Document.Body.InnerHTML = File.Name
	        Set Doc = Word.Documents.Open(File.Path)
		With Word.Selection.Find
			.ClearFormatting
			.Text = "õ"
			.Replacement.ClearFormatting
			.Replacement.Text = "ő"
			.Execute ,,,,,,,wdFindContinue,,,wdReplaceAll
		End With
		With Word.Selection.Find
			.ClearFormatting
			.Text = "û"
			.Replacement.ClearFormatting
			.Replacement.Text = "ű"
			.Execute ,,,,,,,wdFindContinue,,,wdReplaceAll
		End With
	End If
Next

IE.Document.Body.InnerHTML = "Mentés..."
Word.Documents.Save True, wdOriginalDocumentFormat
Word.Quit
IE.Quit
MsgBox("Kész!")